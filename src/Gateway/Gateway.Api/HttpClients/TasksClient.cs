﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Affiliates.Core.Dto;
using Newtonsoft.Json;

namespace Gateway.API.HttpClients
{
    public class TasksClient
    {
        private readonly HttpClient _client;

        public TasksClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<AffiliateTaskDto>> GetAll()
        {
            var response = await _client.GetAsync("Tasks");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<IEnumerable<AffiliateTaskDto>>();
        }

        public async Task<AffiliateTaskDto> Get(int affiliateTaskId)
        {
            var response = await _client.GetAsync("Tasks/" + affiliateTaskId);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<AffiliateTaskDto>();
        }

        public async Task<HttpResponseMessage> Post(AffiliateTaskDto affiliateTask)
        {
            return await _client.PostAsync("Tasks",
                new StringContent(JsonConvert.SerializeObject(affiliateTask), Encoding.UTF8, "application/json"));

        }

        public async Task<HttpResponseMessage> Put(int id, AffiliateTaskDto affiliateTask)
        {
            return await _client.PutAsync("Tasks/" + id,
                new StringContent(JsonConvert.SerializeObject(affiliateTask), Encoding.UTF8, "application/json"));
        }

        public async Task<HttpResponseMessage> Delete(int affiliateTaskId)
        {
            return await _client.DeleteAsync("Tasks/" + affiliateTaskId);
        }
    }
}
