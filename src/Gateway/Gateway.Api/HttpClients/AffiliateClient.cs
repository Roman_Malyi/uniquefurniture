﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Affiliates.Core.Dto;
using Affiliates.Core.Entities;
using Newtonsoft.Json;

namespace Gateway.API.HttpClients
{
    public class AffiliateClient
    {
        private readonly HttpClient _client;

        public AffiliateClient(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<Affiliate>> GetAll()
        {
            var response = await _client.GetAsync("Affiliates");
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<IEnumerable<Affiliate>>();
        }

        public async Task<Affiliate> Get(int affiliateId)
        {
            var response = await _client.GetAsync("Affiliates/" + affiliateId);
            response.EnsureSuccessStatusCode();

            return await response.Content.ReadAsAsync<Affiliate>();
        }

        public async Task<HttpResponseMessage> Post(AffiliateDto affiliate)
        {
            return await _client.PostAsync("Affiliates",
                new StringContent(JsonConvert.SerializeObject(affiliate), Encoding.UTF8, "application/json"));

        }

        public async Task<HttpResponseMessage> Put(int id, AffiliateDto affiliate)
        {
            return await _client.PutAsync("Affiliates/" + id,
                new StringContent(JsonConvert.SerializeObject(affiliate), Encoding.UTF8, "application/json"));
        }

        public async Task<HttpResponseMessage> Delete(int affiliateId)
        {
            return await _client.DeleteAsync("Affiliates/" + affiliateId);
        }

    }
}
