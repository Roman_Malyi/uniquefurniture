﻿using System;
using System.Threading.Tasks;
using Affiliates.Core.Dto;
using Gateway.API.HttpClients;
using Microsoft.AspNetCore.Mvc;

namespace Gateway.API.Controllers
{
    [Route("[controller]")]
    public class TasksController : Controller
    {
        private readonly TasksClient _tasksClient;

        public TasksController(TasksClient tasksClient)
        {
            _tasksClient = tasksClient;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        {
            try
            {
                var result = await _tasksClient.GetAll();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("{taskid}")]
        public async Task<ActionResult> Get(int taskid)
        {
            try
            {
                var result = await _tasksClient.Get(taskid);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody]AffiliateTaskDto affiliateTask)
        {
            try
            {
                var result = await _tasksClient.Post(affiliateTask);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{taskId}")]
        public async Task<ActionResult> Put(int taskId, [FromBody]AffiliateTaskDto affiliateTask)
        {       
            try
            {
                var result = await _tasksClient.Put(taskId, affiliateTask);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{affiliateTaskid}")]
        public async Task<ActionResult> Delete(int affiliateTaskid)
        {
            try
            {
                await _tasksClient.Delete(affiliateTaskid);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
