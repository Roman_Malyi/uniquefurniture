﻿using System;
using System.Threading.Tasks;
using Affiliates.Core.Dto;
using Gateway.API.HttpClients;
using Microsoft.AspNetCore.Mvc;


namespace Gateway.API.Controllers
{
    [Route("[controller]")]
    public class AffiliatesController : Controller
    {
        private readonly AffiliateClient _affiliateClient;

        public AffiliatesController(AffiliateClient affiliateClient)
        {
            _affiliateClient = affiliateClient;
        }

        [HttpGet]
        public async Task<ActionResult> Get()
        { 
            try
            {
                var result = await _affiliateClient.GetAll();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("{affiliateId}")]
        public async Task<ActionResult> Get(int affiliateId)
        {
            try
            {
                var result = await _affiliateClient.Get(affiliateId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody]AffiliateDto value)
        {         
            try
            {
                var result = await _affiliateClient.Post(value);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{affiliateId}")]
        public async Task<ActionResult> Put(int affiliateId, [FromBody]AffiliateDto affiliate)
        {          
            try
            {
                var result = await _affiliateClient.Put(affiliateId, affiliate);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{affiliateId}")]
        public async Task<ActionResult> Delete(int affiliateId)
        {   
            try
            {
                await _affiliateClient.Delete(affiliateId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
