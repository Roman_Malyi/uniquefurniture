﻿using Affiliates.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Affiliates.Data
{
    public sealed class AffiliateDbContext : DbContext
    {
        public AffiliateDbContext(DbContextOptions<AffiliateDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Affiliate> Affiliates { get; set; }
    }
}
