﻿using Affiliates.Core.Data;
using Affiliates.Core.Entities;

namespace Affiliates.Data.Repositories
{
    public class AffiliateRepository : BaseRepository<Affiliate>, IAffiliateRepository
    {
        public AffiliateRepository(AffiliateDbContext context) : base(context){}
    }
}