﻿using System.Linq;
using Affiliates.Core.Data;
using Microsoft.EntityFrameworkCore;

namespace Affiliates.Data.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly DbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        protected BaseRepository(DbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<TEntity>();
        }

        public TEntity Create(TEntity item)
        {
            _dbSet.Add(item);
            _dbContext.SaveChanges();

            return item;
        }

        public TEntity GetById(int id)
        {
            return _dbSet.Find(id);
        }

        public IQueryable<TEntity> GetAll()
        {
            return _dbSet;
        }

        public void Remove(TEntity item)
        {
            _dbSet.Remove(item);
            _dbContext.SaveChanges();
        }

        public TEntity Update(TEntity item)
        {
            _dbSet.Update(item);
            _dbContext.SaveChanges();

            return item;
        }
    }
}
