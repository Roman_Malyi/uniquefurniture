﻿using Affiliates.Core;
using Affiliates.Core.Data;
using Affiliates.Core.Services;
using Affiliates.Data;
using Affiliates.Data.Repositories;
using Affiliates.Services;
using Affiliates.Services.EventBus;
using Affiliates.Services.Services;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Affiliates.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<AffiliateDbContext>(optionBuilder =>
            {
                var connectionString = Configuration.GetConnectionString("DataAccessPostgreSqlProvider");
                optionBuilder.UseNpgsql(connectionString);
            });

            services.Configure<RabbitMqOptions>(Configuration.GetSection("RabbitMq"));

            services.AddScoped<IAffiliateRepository, AffiliateRepository>();
            services.AddScoped<IAffiliateServices, AffiliateService>();
            services.AddAutoMapper(a => a.AddProfile(new MappingProfile()));

            services.AddScoped<AffiliateActionProducer>();

#if DEBUG
            services.AddSwaggerGen(swagger =>
            {
                swagger.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title = "UniqueFurniture" });
            });
#endif
            services.AddOptions();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
#if DEBUG
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "UniqueFurniture");
            });
#endif

            app.UseMvc();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetService<AffiliateDbContext>().Database.EnsureCreated();
            }
        }
    }
}
