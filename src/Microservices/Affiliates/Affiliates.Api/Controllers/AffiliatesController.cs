﻿using System;
using Affiliates.Core.Dto;
using Affiliates.Core.Services;
using Microsoft.AspNetCore.Mvc;

namespace Affiliates.Api.Controllers
{
    [Route("[controller]")]
    public class AffiliatesController : Controller
    {
        private readonly IAffiliateServices _affiliateService;

        public AffiliatesController(IAffiliateServices affiliateService)
        {
            _affiliateService = affiliateService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var result = _affiliateService.GetAll();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("{affiliateId}")]
        public IActionResult Get(int affiliateId)
        {
            try
            {
                var result = _affiliateService.GetById(affiliateId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]AffiliateDto affiliate)
        {
            try
            {
                var result = _affiliateService.Create(affiliate);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{affiliateId}")]
        public IActionResult Put(int affiliateId, [FromBody]AffiliateDto affiliate)
        {
            try
            {
                var result = _affiliateService.Update(affiliateId, affiliate);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{affiliateId}")]
        public IActionResult Delete(int affiliateId)
        {
            try
            {
                _affiliateService.Delete(affiliateId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
