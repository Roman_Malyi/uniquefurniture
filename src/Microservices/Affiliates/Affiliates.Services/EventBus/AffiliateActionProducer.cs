﻿using System.Text;
using Affiliates.Core;
using Affiliates.Core.Dto;
using Affiliates.Core.Entities;
using AutoMapper;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace Affiliates.Services.EventBus
{
    public class AffiliateActionProducer
    {
        private readonly ConnectionFactory _connectionFactory;
        private readonly IMapper _mapper;
        private readonly IOptions<RabbitMqOptions> _rabbitMqOptions;

        public AffiliateActionProducer(IMapper mapper, IOptions<RabbitMqOptions> rabbitMqOptions)
        {
            _rabbitMqOptions = rabbitMqOptions;
            _connectionFactory = new ConnectionFactory()
            {
                HostName = rabbitMqOptions.Value.HostName,
                UserName = rabbitMqOptions.Value.UserName,
                Password = rabbitMqOptions.Value.Password
            };
            _mapper = mapper;    
        }

        private void Send(string message)
        {
            using (var connection = _connectionFactory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: _rabbitMqOptions.Value.QueueName,
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;

                channel.BasicPublish(exchange: "",
                    routingKey: _rabbitMqOptions.Value.QueueName,
                    basicProperties: properties,
                    body: body);
            }

        }

        private string GenerateJsonStringForSending(string eventName, Affiliate affiliate)
        {
            var eventBusAffiliateDto = _mapper.Map<Affiliate, AffiliateChangedEvent>(affiliate);

            eventBusAffiliateDto.EventName = eventName;

            return JsonConvert.SerializeObject(eventBusAffiliateDto);
        }

        public void SendMessageCreate(Affiliate affiliate)
        {
            Send(GenerateJsonStringForSending("Create", affiliate));
        }

        public void SendMessageUpdate(Affiliate affiliate)
        {
            Send(GenerateJsonStringForSending("Update", affiliate));
        }

        public void SendMessageDelete(Affiliate affiliate)
        {
            Send(GenerateJsonStringForSending("Delete", affiliate));
        }
    }
}
