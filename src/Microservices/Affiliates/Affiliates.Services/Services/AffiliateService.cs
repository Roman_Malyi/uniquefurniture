﻿using System;
using System.Collections.Generic;
using Affiliates.Core.Data;
using Affiliates.Core.Dto;
using Affiliates.Core.Entities;
using Affiliates.Core.Services;
using Affiliates.Services.EventBus;
using AutoMapper;

namespace Affiliates.Services.Services
{
    public class AffiliateService : IAffiliateServices
    {
        private readonly IAffiliateRepository _affiliateRepository;
        private readonly IMapper _maper;
        private readonly AffiliateActionProducer _affiliateActionSender;

        public AffiliateService(IAffiliateRepository affiliateRepository, IMapper mapper, AffiliateActionProducer affiliateActionSender)
        {
            _affiliateRepository = affiliateRepository;
            _maper = mapper;
            _affiliateActionSender = affiliateActionSender;
        }

        public Affiliate GetById(int id)
        {
            return _affiliateRepository.GetById(id);
        }

        public Affiliate Create(AffiliateDto entity)
        {
            var newAffiliate = _affiliateRepository.Create(_maper.Map<AffiliateDto, Affiliate>(entity));
            _affiliateActionSender.SendMessageCreate(newAffiliate);
            return newAffiliate;
        }

        public IEnumerable<Affiliate> GetAll()
        {
            return _affiliateRepository.GetAll();
        }

        public Affiliate Update(int id, AffiliateDto entity)
        {
            Affiliate newAffiliate = _maper.Map<AffiliateDto, Affiliate>(entity);
            newAffiliate.Id = id;

            _affiliateActionSender.SendMessageUpdate(_affiliateRepository.Update(newAffiliate));

            return newAffiliate;
        }

        public Affiliate Delete(int id)
        {
            var item = _affiliateRepository.GetById(id);

            if (item != null)
            {
                _affiliateRepository.Remove(item);
                _affiliateActionSender.SendMessageDelete(item);

                return item;
            }
            throw new ArgumentException();
        }
    }
}
