﻿using System.Collections.Generic;
using Affiliates.Core.Dto;
using Affiliates.Core.Entities;

namespace Affiliates.Core.Services
{
    public interface IAffiliateServices
    {
        Affiliate GetById(int id);
        Affiliate Create(AffiliateDto entity);
        IEnumerable<Affiliate> GetAll();
        Affiliate Update(int id, AffiliateDto entity);
        Affiliate Delete(int id);
    }
}
