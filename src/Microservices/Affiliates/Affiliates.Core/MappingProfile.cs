﻿using Affiliates.Core.Dto;
using Affiliates.Core.Entities;
using AutoMapper;

namespace Affiliates.Core
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<AffiliateTaskDto, AffiliateTask>();
            CreateMap<AffiliateTask, AffiliateTaskDto>();

            CreateMap<Affiliate, AffiliateChangedEvent>();
            CreateMap<AffiliateChangedEvent, Affiliate>();

            CreateMap<Affiliate, AffiliateDto>();
            CreateMap<AffiliateDto, Affiliate>();
        }
    }
}
