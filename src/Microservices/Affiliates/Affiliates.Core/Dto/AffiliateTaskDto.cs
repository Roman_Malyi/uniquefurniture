﻿using System;

namespace Affiliates.Core.Dto
{
    public class AffiliateTaskDto
    {
        public DateTime CreatedAt { get; set; }
        public string Message { get; set; }
        public int AffiliateId { get; set; }
    }
}
