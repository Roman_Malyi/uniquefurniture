﻿using System;

namespace Affiliates.Core.Dto
{
    public class AffiliateChangedEvent
    {
        public string EventName { get; set; }
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
