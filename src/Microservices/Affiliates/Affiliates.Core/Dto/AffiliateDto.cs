﻿using System;

namespace Affiliates.Core.Dto
{
    public class AffiliateDto
    {
        public DateTime CreatedAt { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}
