﻿using Affiliates.Core.Entities;

namespace Affiliates.Core.Data
{
    public interface IAffiliateRepository:IRepository<Affiliate>
    {
        
    }
}