﻿using System.Linq;

namespace Affiliates.Core.Data
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Create(TEntity item);
        TEntity GetById(int id);
        IQueryable<TEntity> GetAll();
        void Remove(TEntity item);
        TEntity Update(TEntity item);
    }
}