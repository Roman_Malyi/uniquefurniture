﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Affiliates.Core.Entities
{
    public class AffiliateTask
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Message { get; set; }
        public int AffiliateId { get; set; }
    }
}
