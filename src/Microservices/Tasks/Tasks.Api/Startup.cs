﻿using Affiliates.Core;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Tasks.Api.Data;
using Tasks.Api.Data.Repository;
using Tasks.Api.EventBus;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace Tasks.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddDbContext<TasksDbContext>(optionBuilder =>
            optionBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddAutoMapper(a => a.AddProfile(new MappingProfile()));

            services.Configure<RabbitMqOptions>(Configuration.GetSection("RabbitMq"));

            services.AddScoped<ITasksRepository, TasksRepository>();
            services.AddScoped<AffiliateActionConsumer>();
            
#if DEBUG
            services.AddSwaggerGen(swagger =>
            {
                swagger.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info { Title = "UniqueFurnitureTasks" });
            });


#endif
            services.AddOptions();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

#if DEBUG
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "UniqueFurnitureTasks");
            });
#endif
            var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope();
                serviceScope.ServiceProvider.GetService<TasksDbContext>().Database.EnsureCreated();
                serviceScope.ServiceProvider.GetService<AffiliateActionConsumer>();
            

            app.UseMvc();
        }
    }
}
