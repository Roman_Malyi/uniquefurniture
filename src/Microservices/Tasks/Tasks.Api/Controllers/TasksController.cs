﻿using System;
using Affiliates.Core.Dto;
using Affiliates.Core.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Tasks.Api.Data;
using Tasks.Api.Data.Repository;

namespace Tasks.API.Controllers
{
    [Route("[controller]")]
    public class TasksController : Controller
    {
        private readonly IMapper _mapper;
        private readonly ITasksRepository _tasksRepository;

        public TasksController(TasksDbContext context, IMapper mapper, ITasksRepository tasksRepository)
        {
            _mapper = mapper;
            _tasksRepository = tasksRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var result = _tasksRepository.GetAll();
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("{taskId}")]
        public IActionResult Get(int taskId)
        {
            try
            {
                var result = _tasksRepository.GetById(taskId);
                return Ok(result);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody]AffiliateTaskDto task)
        {  
            try
            {
                _tasksRepository.Create(_mapper.Map<AffiliateTaskDto, AffiliateTask>(task));
                return Ok(task);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("{taskId}")]
        public IActionResult Put(int taskId, [FromBody]AffiliateTaskDto task)
        {  
            try
            {
                var newTask = _mapper.Map<AffiliateTaskDto, AffiliateTask>(task);
                newTask.Id = taskId;
                _tasksRepository.Update(newTask);
                return Ok(newTask);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{taskId}")]
        public IActionResult Delete(int taskId)
        {
            try
            {
                var taskForRemove = _tasksRepository.GetById(taskId);
                _tasksRepository.Remove(taskForRemove);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
