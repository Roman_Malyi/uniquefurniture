﻿using System;
using System.Text;
using Affiliates.Core;
using Affiliates.Core.Dto;
using Affiliates.Core.Entities;
using AutoMapper;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Tasks.Api.Data;
using Tasks.Api.Data.Repository;

namespace Tasks.Api.EventBus
{
    public class AffiliateActionConsumer
    {
        private readonly IMapper _mapper;
        private readonly IOptions<RabbitMqOptions> _rabbitMqOptions;
        private readonly ITasksRepository _tasksRepository;

        public AffiliateActionConsumer(IMapper mapper,IOptions<RabbitMqOptions> rabbitMqOptions, ITasksRepository tasksRepository)
        {
            _mapper = mapper;
            _rabbitMqOptions = rabbitMqOptions;
            _tasksRepository = tasksRepository;
            Listen();
        }

        private void HandleMessage(string message)
        {
            AffiliateChangedEvent affiliateChangedEvent = JsonConvert.DeserializeObject<AffiliateChangedEvent>(message);
            var affiliate = _mapper.Map<AffiliateChangedEvent, Affiliate>(affiliateChangedEvent);

            switch (affiliateChangedEvent.EventName)
            {
                case "Create":
                {
                    SendAffiliateCreateTasks(affiliate);
                    break;
                }
                case "Update":
                {
                    SendAffiliateUpdateTasks(affiliate);
                    break;
                }
                case "Delete":
                {
                    SendAffiliateDeleteTasks(affiliate);
                    break;
                }
            }
        }

        private void SendAffiliateCreateTasks(Affiliate affiliate)
        {
            CreateTask(affiliate, "Buy furniture.");
            CreateTask(affiliate, "Hire workers.");
        }

        private void SendAffiliateUpdateTasks(Affiliate affiliate)
        {
            CreateTask(affiliate, "Change Signboard.");
        }

        private void SendAffiliateDeleteTasks(Affiliate affiliate)
        {
            var tasks = _tasksRepository.GetAll();

            foreach (var task in tasks)
            {
                if (task.AffiliateId == affiliate.Id)
                {
                    _tasksRepository.Remove(task);
                }
            }
        }

        private void CreateTask(Affiliate affiliate, string message)
        {
            var newTask = new AffiliateTask() {AffiliateId = affiliate.Id, CreatedAt = DateTime.Now, Message = message};
            _tasksRepository.Create(newTask);
        }

        public void Listen()
        {
            var factory = new ConnectionFactory()
            {
                HostName = _rabbitMqOptions.Value.HostName,
                UserName = _rabbitMqOptions.Value.UserName,
                Password = _rabbitMqOptions.Value.Password
            };
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            channel.QueueDeclare(queue: _rabbitMqOptions.Value.QueueName,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body;
                var message = Encoding.UTF8.GetString(body);

                HandleMessage(message);

                channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
            };
            channel.BasicConsume(queue: _rabbitMqOptions.Value.QueueName,
                autoAck: false,
                consumer: consumer);
        }
    }
}
