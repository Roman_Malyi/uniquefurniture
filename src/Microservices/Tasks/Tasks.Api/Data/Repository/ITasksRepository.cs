﻿using Affiliates.Core.Data;
using Affiliates.Core.Entities;

namespace Tasks.Api.Data.Repository
{
    public interface ITasksRepository : IRepository<AffiliateTask>
    {
    }
}
