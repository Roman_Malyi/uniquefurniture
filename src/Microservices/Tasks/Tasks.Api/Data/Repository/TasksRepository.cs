﻿using Affiliates.Core.Entities;
using Affiliates.Data.Repositories;
using Microsoft.EntityFrameworkCore;
using Tasks.Api.Data.Repository;

namespace Tasks.Api.Data
{
    public class TasksRepository:BaseRepository<AffiliateTask>, ITasksRepository
    {
        public TasksRepository(TasksDbContext dbContext) : base(dbContext)
        {
        }
    }
}
