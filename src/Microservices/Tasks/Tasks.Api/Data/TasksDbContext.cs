﻿using Affiliates.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace Tasks.Api.Data
{
    public sealed class TasksDbContext : DbContext
    {
        public TasksDbContext(DbContextOptions<TasksDbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<AffiliateTask> Tasks { get; set; }

    }
}
